import { LoginModal } from './classes/LoginModal.js';
import { CreateVisitModal } from './classes/visit/CreateVisitModal.js';
import { LoginUser } from './classes/LoginUser.js';
import { VisitCard } from './classes/VisitCard.js';
import getCards from './api/getCards.js';
import { CardsFilter } from './classes/CardsFilter.js';

export async function handleLoginUser() {
  const createVisitBtn = document.querySelector(`[data-modal-target="modalCreateVisit"]`);
  const loginBtn = document.querySelector(`[data-modal-target="modalSignIn"]`);
  const sectionFilter = document.querySelector('.section-filter');
  const noCards = document.querySelector('#no-cards');

  const userToken = localStorage.getItem('token');

  if (userToken) {
    const loginInit = new LoginUser();
    loginInit.init();

    const visitCardsArr = await getCards();
    visitCardsArr.forEach((element) => {
      const card = new VisitCard(element);
      console.log(element);
      card.init();
    });

    const filter = new CardsFilter(visitCardsArr);
    filter.init();

    sectionFilter.classList.remove('d-none');
    loginBtn.classList.add('d-none');
    noCards.classList.add('d-block');

    createVisitBtn.classList.remove('d-none');

    const createVisitModal = new CreateVisitModal('modalCreateVisit');
    createVisitModal.init();
    
  } else {
    const loginModal = new LoginModal('modalSignIn');
    loginModal.init();
  }
}

handleLoginUser();
