import Visit from './Visit.js';

export default class VisitTeraphist extends Visit {
    constructor(
        parentElement,
        editAction,
        deleteAction,
        age,
        desc,
        doctor,
        fullName,
        urgency,
        purpose,
        status,
        heartIllness,
        id,
        pressure,
        weightIndex,
        lastDateVisit,
    ) {
        super('Терапевт', parentElement, editAction, deleteAction)
        this.age = age;
        this.desc = desc;
        this.fullName = fullName;
        this.urgency = urgency;
        this.purpose = purpose;
        this.status = status;
        this.heartIllness = heartIllness;
        this.id = id;
        this.pressure = pressure;
        this.weightIndex = weightIndex;
        this.lastDateVisit = lastDateVisit;

        this.ageContainer = document.createElement('p');
        this.ageInput = document.createElement('input');

        this.descContainer = document.createElement('p');
        this.descInput = document.createElement('input');

        this.fullNameContainer = document.createElement('p');
        this.fullNameInput = document.createElement('input');

        this.urgencyContainer = document.createElement('p');
        this.urgencySelect = document.createElement('select')

        this.purposeContainer = document.createElement('p');
        this.purposeInput = document.createElement('input');

        this.statusContainer = document.createElement('p');
        this.statusSelect = document.createElement('select');

        this.heartIllnessContainer =document.createElement('p');
        this.heartIllnessInput = document.createElement('input');

        this.pressureContainer = document.createElement('p');
        this.pressureInput = document.createElement('input');

        this.weightIndexContainer = document.createElement('p');
        this.weightIndexInput = document.createElement('input');

        this.lastDateVisitContainer = document.createElement('p');
        this.lastDateVisitInput = document.createElement('input');
    }

    createEl() {
        super.createEl();

        this.contentContainer.parentNode.setAttribute('card-id', this.id);

        this.fullNameContainer.innerHTML = `<span class = "visit__fild-name">ФІО: </span>`;
        this.fullNameInput.classList.add('form-control');

        this.ageContainer.innerHTML = `<span class = "visit__fild-name">Вік: </span>`;
        this.ageInput.classList.add('form-control')

        this.descContainer.innerHTML = `<span class = "visit__fild-name">Опис візіиту: </span>`;
        this.descInput.classList.add('form-control')

        this.urgencyContainer.innerHTML = `<span class = "visit__fild-name">Терміновість: </span>`;
        this.urgencySelect.classList.add('form-select')
        this.urgencySelect.innerHTML = `<select><option value='Low'>Звичайна</option><option value='Normal'>Пріоритетна</option><option value='High'>Невідкладна</option></select>`

        this.purposeContainer.innerHTML = `<span class = "visit__fild-name">Ціль візиту: </span>`;
        this.purposeInput.classList.add('form-control');

        this.statusContainer.innerHTML = `<span class = "visit__fild-name">Статус: </span>`;
        this.statusSelect.classList.add('form-select')
        this.statusSelect.innerHTML = `<select><option value='Open'>Open</option><option value='Done'>Done</option></select>`

        
        this.contentContainer.append(
            this.fullNameContainer,
            this.fullNameInput,

            this.ageContainer,
            this.ageInput,

            this.descContainer,
            this.descInput,

            this.urgencyContainer,
            this.urgencySelect,

            this.purposeContainer,
            this.purposeInput,

            this.statusContainer,
            this.statusSelect
        );
    }

    getData() {
        const baseVisitData = super.getData();
        return {
            ...baseVisitData,
            id: this.id,
            name: this.fullNameInput.value,
            age: this.ageInput.value,
            desc: this.descInput.value,
            urgency: this.urgencySelect.value,
            purpose: this.purposeInput.value,
            status: this.statusSelect.value
        };
    }

    clear() {
        this.fullNameInput.value = '';
        this.ageInput.value = '';
        this.descInput.value = '';
        this.urgencySelect.value = '';
        this.purposeInput.value = '';
        this.statusSelect.value = '';
    }
}