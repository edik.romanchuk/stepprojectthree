import { Modal } from '../Modal.js';
import VisitCardiologist from './VisitCardiologist.js';
import VisitDentist from './VisitDentist.js';
import VisitTeraphist from './VisitTeraphist.js';
import { VisitCard } from '../VisitCard.js';

export class CreateVisitModal extends Modal {
  constructor(modalName) {
    super(modalName);
    this.formDoctorSelect = document.querySelector('form-select');
    this.doctorContainer = document.createElement('div');
    this.doctorControl = null;
    this.formContainer = document.createElement('div');
    this.labelElement = document.createElement('label');
    this.doctorSelect = document.createElement('select');
  }

  _createElements() {
    super._createElements();

    this.modalTitle.innerText = 'Створити новий візит';

    this.formContainer.classList.add('form-container');

    this.labelElement.innerText = 'Вибери Лікаря:';

    this.doctorSelect.classList.add('form-select');
    this.doctorSelect.innerHTML =
      "<option value='dentist'>Дантист</option><option value='cardiolog'>Кардіолог</option><option value='terapevt'>Терапевт</option>";
    this.doctorSelect.onchange = this.onDoctorChange;

    this.formContainer.append(this.labelElement);
    this.formContainer.append(this.doctorSelect);

    this.formContainer.append(this.doctorContainer);

    this.modalForm.prepend(this.formContainer);
  }

  onSubmit(e) {
    super.onSubmit(e);
    const doctorData = this.doctorControl.getData();

    fetch('https://ajax.test-danit.com/api/v2/cards', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(doctorData),
    })
      .then((response) => {
        this.doctorControl.clear();

        super.hideModal();
        if (response.ok) return response.json();
      })
      .then((res) => {
        const card = new VisitCard(res);
        card.init();
      })
      .catch((e) => {
        alert('Error, please try again.');
        console.error(e, 'error');
      });
  }

  render() {
    super.render();

    this.displayDoctorElements('dentist');
  }

  onDoctorChange = (e) => {
    const selectedValue = e.target.value;

    this.displayDoctorElements(selectedValue);
  };

  displayDoctorElements(selectedDoctor) {
    switch (selectedDoctor) {
      case 'dentist':
        this.doctorControl = new VisitDentist(this.doctorContainer);
        break;
      case 'cardiolog':
        this.doctorControl = new VisitCardiologist(this.doctorContainer);
        break;
      case 'terapevt':
        this.doctorControl = new VisitTeraphist(this.doctorContainer);
        break;
    }

    this.doctorControl.createEl();
  }
  init() {
    super._addEventListeners();
  }
}
