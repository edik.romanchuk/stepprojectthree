
export default class Visit {
    constructor(doctor, parentElement, editAction, deleteAction) {
        this.doctor = doctor;
        this.parentElement = parentElement;

        this.divCard = document.createElement('div');
        this.btnEdit = document.createElement('button');
        this.btnDelete = document.createElement('button');
        this.editAction = editAction;
        this.deleteAction = deleteAction;
        this.contentContainer = document.createElement('div');
    }

    createEl() {
        this.contentContainer.classList.add('card__content-container');

        this.divCard.append(this.btnEdit);
        this.divCard.append(this.btnDelete);
        this.divCard.append(this.contentContainer);
        this.divCard.className = 'card';
        
        this.btnEdit.className = 'btn btn-primary card__btn card__edit';
        this.btnEdit.innerText = 'Змінити';
        this.btnEdit.setAttribute('type', 'button')
        this.btnEdit.setAttribute('data-bs-toggle', 'modal');
        this.btnEdit.setAttribute('data-bs-target', '#modalChangeCard');

        this.btnDelete.className = 'card__btn card__delete';

        this.parentElement.innerHTML = '';
        this.parentElement.append(this.contentContainer);
        
    }

    getData() {
        return {
            doctor: this.doctor
        };
    }

    clear() {}
}