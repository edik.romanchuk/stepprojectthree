export class Modal {
  constructor(modalName) {
    this.modalName = modalName;
    this.showBtn = document.querySelector(`[data-modal-target="${modalName}"]`);
    this.modalWrapper = document.createElement('div');
    this.modalDialog = document.createElement('div');
    this.modalContent = document.createElement('div');
    this.modalHeader = document.createElement('div');
    this.modalTitle = document.createElement('h1');
    this.modalCloseBtn = document.createElement('button');
    this.modalBody = document.createElement('div');
    this.modalForm = document.createElement('form');
    this.modalSubmit = document.createElement('button');
  }

  onSubmit(e) {
    e.preventDefault();
  }

  showModal() {
    this.render();
  }

  hideModal() {
    this.modalWrapper.remove();
  }

  _createElements() {
    this.modalWrapper.classList.add('modal', 'modal-bg', 'py-5', 'active-modal');
    this.modalWrapper.tabIndex = '-1';
    this.modalWrapper.role = 'dialog';
    this.modalWrapper.id = this.modalName;

    this.modalDialog.role = 'document';
    this.modalDialog.classList.add('modal-dialog');

    this.modalContent.classList.add('modal-content', 'rounded-4', 'shadow');
    this.modalHeader.classList.add('modal-header', 'p-5', 'pb-4', 'border-bottom-0');

    this.modalTitle.classList.add('modal-title', 'fs-5');
    this.modalTitle.innerText = 'Modal title';

    this.modalCloseBtn.classList.add('btn-close');
    this.modalCloseBtn.type = 'button';
    this.modalCloseBtn.dataset.btn = this.modalName;
    this.modalCloseBtn.ariaLabel = 'Close';

    this.modalBody.classList.add('modal-body', 'p-5', 'pt-0');

    this.modalSubmit.classList.add('w-100', 'mb-2', 'btn', 'btn-lg', 'rounded-3', 'btn-primary');
    this.modalSubmit.type = 'submit';
    this.modalSubmit.innerText = 'Submit';

    this.modalWrapper.append(this.modalDialog);
    this.modalDialog.append(this.modalContent);
    this.modalContent.append(this.modalHeader);
    this.modalContent.append(this.modalBody);
    this.modalHeader.append(this.modalTitle);
    this.modalHeader.append(this.modalCloseBtn);
    this.modalBody.append(this.modalForm);
    this.modalForm.append(this.modalSubmit);
  }

  _addEventListeners() {
    this.showBtn?.addEventListener('click', this.showModal.bind(this));
    this.modalCloseBtn.addEventListener('click', this.hideModal.bind(this));
    this.modalSubmit.addEventListener('click', this.onSubmit.bind(this));
  }

  render(parentElement = document.body) {
    this._createElements();
    parentElement.insertAdjacentElement('beforeend', this.modalWrapper);
  }
}
