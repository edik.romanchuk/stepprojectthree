import EditVisitModal from './EditVisitModal.js';
import VisitCardiologist from './visit/VisitCardiologist.js';
import VisitDentist from './visit/VisitDentist.js';
import VisitTeraphist from './visit/VisitTeraphist.js';

export class VisitCard {
  constructor({
    desc,
    doctor,
    id,
    lastDateVisit,
    name,
    purpose,
    status,
    urgency,
    age,
    heartIllness,
    pressure,
    weightIndex,
  }) {
    this.desc = desc;
    this.doctor = doctor;
    this.id = id;
    this.lastDateVisit = lastDateVisit;
    this.fullName = name;
    this.purpose = purpose;
    this.status = status;
    this.urgency = urgency;
    this.age = age;
    this.heartIllness = heartIllness;
    this.pressure = pressure;
    this.weightIndex = weightIndex;

    this.cardContainer = document.querySelector('#card-container');
    this.noCards = document.querySelector('#no-cards');

    this.cardWrapper = document.createElement('div');
    this.card = document.createElement('div');
    this.cardBody = document.createElement('div');
    this.titleWrapper = document.createElement('div');
    this.cardPatientFullName = document.createElement('h5');
    this.cardDeleteBtn = document.createElement('button');
    this.cardDoctorProfile = document.createElement('p');
    this.cardBtnWrapper = document.createElement('div');
    this.cardBtnViewMore = document.createElement('a');
    this.cardBtnEdit = document.createElement('a');
    this.cardContainer = document.querySelector('#card-container');
    this.token = localStorage.getItem('token');

    this.cardDescriptionWrapper = document.createElement('div');
    this.cardDescriptionDesc = document.createElement('p');
    this.cardDescriptionUrgency = document.createElement('p');
    this.cardDescriptionDescPurpose = document.createElement('p');
    this.cardDescriptionDescStatus = document.createElement('p');
    this.cardDescriptionDesc = document.createElement('p');
    this.cardDescriptionLastVisit = document.createElement('p');
    this.cardDescriptionAge = document.createElement('p');
    this.cardDescriptionHeartIllness = document.createElement('p');
    this.cardDescriptionPressure = document.createElement('p');
    this.cardDescriptionWeightIndex = document.createElement('p');

    this.doctorContainer = document.createElement('div');
  }

  _createElements() {

    this.cardWrapper.classList.add('col-sm-4', 'mb-4');
    this.card.classList.add('card');
    this.cardBody.classList.add('card-body');
    this.titleWrapper.classList.add('d-flex', 'justify-content-between');
    this.cardPatientFullName.classList.add('card-title');
    this.cardDeleteBtn.classList.add('btn-close');
    this.cardDoctorProfile.classList.add('card-text');
    this.cardBtnWrapper.classList.add('d-flex', 'justify-content-between');
    this.cardBtnViewMore.classList.add('btn', 'btn-primary');
    this.cardBtnEdit.classList.add('btn', 'btn-primary');
    this.cardBtnEdit.dataset.modalTarget = 'editCard';
    this.cardDescriptionWrapper.classList.add('d-none');

    this.cardPatientFullName.innerText = this.fullName;
    this.cardDoctorProfile.innerText = this.doctor;

    this.cardBtnEdit.innerText = 'Редагувати';
    this.cardBtnViewMore.innerText = 'Детальніше';

    this.cardContainer.append(this.cardWrapper);
    this.cardWrapper.append(this.card);
    this.card.append(this.cardBody);
    this.cardBody.append(this.titleWrapper);
    this.cardBody.append(this.cardDoctorProfile);
    this.cardBody.append(this.cardDescriptionWrapper);
    this.titleWrapper.append(this.cardPatientFullName);
    this.titleWrapper.append(this.cardDeleteBtn);
    this.cardBody.append(this.cardBtnWrapper);
    this.cardBtnWrapper.append(this.cardBtnViewMore);
    this.cardBtnWrapper.append(this.cardBtnEdit);
  }

  _addEventListeners() {
    this.cardDeleteBtn.addEventListener('click', this.cardDeleteAction.bind(this));
    this.cardBtnEdit.addEventListener('click', this.cardEditBtn.bind(this));
    this.cardBtnViewMore.addEventListener('click', this.cardViewMoreBtn.bind(this));
  }

  async cardDeleteAction() {
    const deleteResponse = await fetch(`https://ajax.test-danit.com/api/v2/cards/${this.id}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    });

    if (deleteResponse.ok) {
      this.cardWrapper.remove();
    }
    if (this.cardContainer.hasChildNodes()) {
      this.noCards.classList.add('d-none');
    } else {
      this.noCards.classList.remove('d-none');
    }
  }

  cardViewMoreBtn() {
    if (this.cardBtnViewMore.innerText === 'Менше') {
      this.cardBtnViewMore.innerText = 'Детальніше';
      this.cardDescriptionWrapper.classList.add('d-none');
    } else {
      this.cardDescriptionWrapper.classList.remove('d-none');
      this.cardBtnViewMore.innerText = 'Менше';
      this.cardDescriptionDesc.innerText = `Опис: ${this.desc}`;
      this.cardDescriptionUrgency.innerText = `Терміновість: ${this.urgency}`;
      this.cardDescriptionDescPurpose.innerText = `Ціль візиту: ${this.purpose}`;
      this.cardDescriptionDescStatus.innerText = `Статус: ${this.status}`;

      switch (this.doctor) {
        case 'Дантист':
          this.cardDescriptionLastVisit.innerText = `Останній візит ${this.lastDateVisit}`;

          this.cardDescriptionWrapper.append(this.cardDescriptionDesc);
          this.cardDescriptionWrapper.append(this.cardDescriptionUrgency);
          this.cardDescriptionWrapper.append(this.cardDescriptionDescPurpose);
          this.cardDescriptionWrapper.append(this.cardDescriptionDescStatus);
          this.cardDescriptionWrapper.append(this.cardDescriptionLastVisit);
        break;
        case 'Кардіолог':
          this.cardDescriptionAge.innerText = `Вік: ${this.age}`;
          this.cardDescriptionHeartIllness.innerText = `Захворювання серця: ${this.heartIllness}`;
          this.cardDescriptionPressure.innerText = `Тиск: ${this.pressure}`;
          this.cardDescriptionWeightIndex.innerText = `Індекс маси тіла: ${this.weightIndex}`;

          this.cardDescriptionWrapper.append(this.cardDescriptionAge);
          this.cardDescriptionWrapper.append(this.cardDescriptionDesc);
          this.cardDescriptionWrapper.append(this.cardDescriptionUrgency);
          this.cardDescriptionWrapper.append(this.cardDescriptionDescPurpose);
          this.cardDescriptionWrapper.append(this.cardDescriptionDescStatus);
          this.cardDescriptionWrapper.append(this.cardDescriptionHeartIllness);
          this.cardDescriptionWrapper.append(this.cardDescriptionPressure);
          this.cardDescriptionWrapper.append(this.cardDescriptionWeightIndex);
        break;
        case 'Терапевт':
          this.cardDescriptionAge.innerText = `Вік: ${this.age}`;

          this.cardDescriptionWrapper.append(this.cardDescriptionDesc);
          this.cardDescriptionWrapper.append(this.cardDescriptionUrgency);
          this.cardDescriptionWrapper.append(this.cardDescriptionDescPurpose);
          this.cardDescriptionWrapper.append(this.cardDescriptionDescStatus);
          this.cardDescriptionWrapper.append(this.cardDescriptionLastVisit);
          this.cardDescriptionWrapper.append(this.cardDescriptionAge);
        break;
        default:
          alert(`Unknown doctor: '${this.doctor}'.`);
        return;
      }
    }
  }

  cardEditBtn() {
    const editVisitModal = new EditVisitModal('Edit visit');
    editVisitModal.init();

    let doctorControl = null;
    const doctorContainer = editVisitModal.getDoctorContainer();
    debugger;
    switch (this.doctor) {
      case 'Дантист':
        doctorControl = new VisitDentist(doctorContainer, this.cardPatientFullName, this.cardDescriptionDesc, this.cardDescriptionUrgency, this.cardDescriptionDescPurpose, this.cardDescriptionDescStatus, this.cardDescriptionLastVisit, this.id);
      break;
      case 'Кардіолог':
        doctorControl = new VisitCardiologist(doctorContainer, this.cardPatientFullName, this.cardDescriptionAge, this.cardDescriptionDesc, this.cardDescriptionUrgency, this.cardDescriptionDescPurpose, this.cardDescriptionDescStatus, this.cardDescriptionHeartIllness, this.cardDescriptionPressure, this.cardDescriptionWeightIndex, this.id);
      break;
      case 'Терапевт':
        doctorControl = new VisitTeraphist(doctorContainer, this.cardPatientFullName, this.cardDescriptionDesc, this.cardDescriptionUrgency, this.cardDescriptionDescPurpose, this.cardDescriptionDescStatus, this.cardDescriptionLastVisit, this.cardDescriptionAge, this.id);
      break;
      default:
        alert(`Unknown doctor: '${this.doctor}'.`);
        return;
    }

    editVisitModal.displayDoctorElements(doctorControl);
    editVisitModal.showModal();
  }

  init() {
    this._createElements();
    this._addEventListeners();

    if (this.cardContainer.hasChildNodes()) {
      this.noCards.classList.add('d-none');
    } else {
      this.noCards.classList.add('d-block');
    }
  }
}