import { Modal } from './Modal.js';

export default class EditVisitModal extends Modal {
  constructor(modalName) {
    super(modalName);

    this.doctorContainer = document.createElement('div');
    this.doctorControl = null;
  }

  _createElements() {
    super._createElements();

    this.modalTitle.innerText = 'Редагувати візит';

    const formContainer = document.createElement('div');
    formContainer.append(this.doctorContainer);

    this.modalForm.prepend(formContainer);
  }

  onSubmit(e) {
    super.onSubmit(e);
    debugger;
    const doctorData = this.doctorControl.getData();
    debugger;
    fetch(`https://ajax.test-danit.com/api/v2/cards/${doctorData.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(doctorData),
    })
    .then((response) => {


      super.hideModal();
      if (response.ok) return response.json();
    })
    .catch((e) => {
      alert('Error, please try again.');
      console.error(e, 'error');
    });
  }

  render() {
    super.render();
  }

  displayDoctorElements(visitControl) {
    this.doctorControl = visitControl;
    this.doctorControl.createEl();
  }

  getDoctorContainer() {
    return this.doctorContainer;
  }

  init() {
    super._addEventListeners();
  }
}
