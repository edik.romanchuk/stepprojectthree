import { Modal } from './Modal.js';
import getCards from '../api/getCards.js';

export class LoginUser extends Modal {
  constructor() {
    super();
    this.email = document.querySelector('#loginInput');
    this.pass = document.querySelector('#passwordInput');
    this.token = localStorage.getItem('token');
  }

  async init() {
    if (localStorage.getItem('token')) {
      await getCards();
    } else {
      await this.loginUser();
      await getCards();
    }
  }

  loginUser() {
    return fetch('https://ajax.test-danit.com/api/v2/cards/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: this.email.value, password: this.pass.value }),
    })
      .then((r) => {
        return r.text();
      })
      .then((token) => {
        localStorage.setItem('token', this.token);
      })
      .catch((e) => {
        console.error(e, 'login User err');
      });
  }

  onSubmit() {
    super.onSubmit();
    this.init();
  }
}
