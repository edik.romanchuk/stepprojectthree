import { VisitCard } from './VisitCard.js';

export class CardsFilter {
  constructor(cardsArr) {
    this.statusFilterDropDown = document.querySelector('#selectStatus');
    this.urgencyFilterDropDown = document.querySelector('#selectUrgency');
    this.cardContainer = document.querySelector('#card-container');
    this.searchInput = document.querySelector('#inputFilter');
    this.allCards = document.querySelectorAll('.card');
    this.cardsArr = cardsArr;
    this.status = 'None';
    this.urgency = 'None';
    this.cards = [];
  }

  renderFilteredCards() {
    this.cardsArr
      .filter((card) => {
        if (
          (this.status === 'None' && this.urgency === card.urgency) ||
          (this.urgency === 'None' && this.status === card.status) ||
          (this.status === 'None' && this.urgency === 'None') ||
          (this.status === card.status && this.urgency === card.urgency)
        ) {
          this.cardContainer.innerHTML = '';
          return card;
        } else {
          this.cardContainer.innerHTML = '';
        }
      })
      .forEach((element) => {
        const card = new VisitCard(element);
        card.init();
      });
  }

  filterByStatus(e) {
    this.status = e.target.value;
    this.renderFilteredCards();
  }

  filterByUrgency(e) {
    this.urgency = e.target.value;
    this.renderFilteredCards();
  }

  filterByTitle(e) {
    const inputText = e.target.value.toLowerCase();
    this.cardContainer.innerHTML = '';

    const filteredInput = this.cardsArr.filter((card) => {
      for (let value of Object.values(card)) {
        if (String(value).toLowerCase().includes(inputText)) {
          return true;
        }
      }
    });
    filteredInput.forEach((card) => {
      new VisitCard(card).init();
    });
  }

  _addEventListeners() {
    this.statusFilterDropDown.addEventListener('change', this.filterByStatus.bind(this));
    this.urgencyFilterDropDown.addEventListener('change', this.filterByUrgency.bind(this));
    this.searchInput.addEventListener('input', this.filterByTitle.bind(this));
  }
  init() {
    this._addEventListeners();
  }
}
