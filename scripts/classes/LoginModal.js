import { Modal } from './Modal.js';
import { handleLoginUser } from '../script.js';

export class LoginModal extends Modal {
  constructor(modalName) {
    super(modalName);
    this.emailInputWrapper = document.createElement('div');
    this.passwordInputWrapper = document.createElement('div');
    this.emailInput = document.createElement('input');
    this.passwordInput = document.createElement('input');
    this.emailInputLabel = document.createElement('label');
    this.passwordInputLabel = document.createElement('label');

    this.successLoginButton = document.createElement('button');
  }
  _createElements() {
    super._createElements();
    this.modalTitle.innerText = 'Увійдіть в систему';

    this.emailInputWrapper.classList.add('form-floating', 'mb-3');
    this.passwordInputWrapper.classList.add('form-floating', 'mb-3');
    this.emailInput.id = 'loginInput';
    this.passwordInput.id = 'passwordInput';
    this.emailInput.placeholder = 'name@example.com';
    this.passwordInput.placeholder = 'Password';
    this.passwordInput.type = 'password';
    this.emailInput.type = 'email';
    this.emailInput.classList.add('form-control', 'rounded-3');
    this.passwordInput.classList.add('form-control', 'rounded-3');
    this.emailInputLabel.htmlFor = 'loginInput';
    this.emailInputLabel.innerText = 'Email address';
    this.passwordInputLabel.innerText = 'Password';
    this.passwordInputLabel.htmlFor = 'passwordInput';

    this.modalForm.prepend(this.emailInputWrapper, this.passwordInputWrapper);

    this.emailInputWrapper.append(this.emailInput, this.emailInputLabel);
    this.passwordInputWrapper.append(this.passwordInput, this.passwordInputLabel);
  }
  onSubmit(e) {
    super.onSubmit(e);
    if (this.emailInput.value !== '' && this.passwordInput.value !== '') {
      fetch('https://ajax.test-danit.com/api/v2/cards/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email: this.emailInput.value, password: this.passwordInput.value }),
      })
        .then((r) => {
          if (!r.ok) {
            alert('Incorrect data');
            return;
          }
          return r.text();
        })
        .then((token) => {
          super.hideModal();
          if (token) {
            localStorage.setItem('token', token);
            this.showBtn.classList.add('d-none');
            handleLoginUser();
          }
        })
        .catch((e) => {
          console.error(e, 'login User err');
        });
    }
  }

  init() {
    this._createElements();
    this._addEventListeners();
  }

  render() {
    this._createElements();
    super.render();
  }
}
